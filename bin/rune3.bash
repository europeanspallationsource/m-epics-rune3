
declare -a params;

ENV=""
while [ "$#" -gt 0 ]; do
	param="$1";
	case "$param" in
		--e3-epics-version=*)
			export E3_EPICS_VERSION=${1##--e3-epics-version=}
			;;

		--e3-epics-version)
			shift;
			export E3_EPICS_VERSION="$1"
			;;

		--e3-version=*)
			export E3_VERSION=${1##--e3-version=}
			;;

		--e3-version)
			shift;
			export E3_VERSION="$1"
			;;

		--env=*)
			ENV=${1##--env=}
			;;

		--env)
			shift;
			ENV="$1";
			;;

		*)
			params+="$1"
			;;
	esac
	shift;
done

E3_ROOT="/epics"

if [ ! -z "$ENV" ]; then
	test -f "$ENV" || { echo "Cannot find $ENV"; kill $PPID; }
	set -a;
	source ${ENV}
	set +a;
fi

echo "source ${E3_ROOT}/base-${E3_EPICS_VERSION}/require/${E3_VERSION}/bin/setE3Env.bash"
source ${E3_ROOT}/base-${E3_EPICS_VERSION}/require/${E3_VERSION}/bin/setE3Env.bash

echo "${E3_ROOT}/base-${E3_EPICS_VERSION}/require/${E3_VERSION}/bin/iocsh.bash ${params[@]}"
${E3_ROOT}/base-${E3_EPICS_VERSION}/require/${E3_VERSION}/bin/iocsh.bash ${params[@]}

echo "Terminating parent IOC..."
kill $PPID
