#@field ST_CMD
#@type STRING
#Path to script to run

#@field E3_EPICS_VERSION
#@type STRING
#Version of E3's EPICS base

#@field E3_VERSION
#@type STRING
#Version of E3's require

#@field E3_ENV
#@type STRING
#Path to optional env file to source

#@field REQUIRE_rune3_PATH
#@runtime YES

# If E3_ENV is set, then this command will succeed and terminate _this_ IOC upon exit
system "/bin/bash $(REQUIRE_rune3_PATH)/$(EPICSVERSION)/bin/$(EPICS_HOST_ARCH)/rune3.bash --e3-epics-version=$(E3_EPICS_VERSION) --e3-version=$(E3_VERSION) --env=$(E3_ENV) $(ST_CMD)"

# If we get here it means that one/some of the macros are not set. We assume it is E3_ENV. So let's try again without that one
system "/bin/bash $(REQUIRE_rune3_PATH)/$(EPICSVERSION)/bin/$(EPICS_HOST_ARCH)/rune3.bash --e3-epics-version=$(E3_EPICS_VERSION) --e3-version=$(E3_VERSION) $(ST_CMD)"
